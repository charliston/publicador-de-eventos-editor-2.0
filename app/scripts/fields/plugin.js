function getObjects(obj, key, val) {
  var objects = [];
  for (var i in obj) {
    if (!obj.hasOwnProperty(i)) continue;
    if (typeof obj[i] == 'object') {
      objects = objects.concat(getObjects(obj[i], key, val));
    } else if (i == key && obj[key] == val) {
      objects.push(obj);
    }
  }
  return objects[0];
}

(function() {
  CKEDITOR.plugins.add('gendoc-normal', {
    requires : ['iframedialog', 'widget'],
    init : function(editor) {
      // custom css
      CKEDITOR.config.contentsCss =  'scripts/fields/styles/contents.css';
      // permitir os campos no editor
      editor.filter.allow( 'input(!gendoc-campo-normal)[!readonly,!disabled,!value,!type,!data-*];'+
        'input(!gendoc-campo-content); p(*)[*]{*};', 'gendoc-normal' );
      // We will start from registering the widget dialog window by calling the standard CKEDITOR.dialog.add method inside the init method of the widget plugin definition.
      var iframeWindow = null;
      var me = this;
      CKEDITOR.dialog.add('normal_dialog', function() {
        return {
          title : 'Campo normal louco',
          resizable: CKEDITOR.DIALOG_RESIZE_NONE,
          minWidth: 200,
          minHeight: 230,
          contents : [{
            id : 'iframe',
            label : 'Campo normal',
            expand : true,
            elements : [{
              id : 'myIframe',
              type : 'iframe',
              src : 'scripts/fields/dialogs/normal.html',
              width : '100%',
              height : '200px',
              onContentLoad : function() {
                var iframe = document.getElementById(this._.frameId);
                iframeWindow = iframe.contentWindow;

                var $iframe = $('#'+this._.frameId).contents();

                // can now call methods in the iframe window

                var $campos = $iframe.find("#campos");
                var $identificacao = $iframe.find('#identificacao');


                // campos disponiveis
                $.each(GENDOC_EVENT_FIELDS, function(i, v) {
                  $campos.append($("<option />").val(v.field_form_id).text(v.field_name));
                });

                // Try and get the current selected element
                var selection = editor.getSelection();
                var element = ((selection.getSelectedElement() != null) ? $(selection.getSelectedElement().getHtml()) : null);

                // If an image is selected then set the appropriate values
                if (element != null && element[0].tagName == 'INPUT' && element.hasClass('gendoc-campo-normal')) {
                  $campos.val(element.data('identifier'));
                  $identificacao.val(element.data('nomeinterno'));
                }
              }
            }]
          }],
          onOk : function() {

            var myIframe = this.getContentElement('iframe', 'myIframe');
            // can now interrogate values in the iframe, call javascript methods
            // can also call editor methods, e.g. editor.focus(), editor.getSelection()
            var $iframe = $('#'+myIframe.domId).contents();

            var $campos = $iframe.find("#campos");
            var $identificacao = $iframe.find('#identificacao');

            var identifier = $campos.val();
            var nomeinterno = $identificacao.val();
            var camponome = $campos.children(':selected').text();
            var value = camponome.toUpperCase();

            if( identifier != '' && nomeinterno != ''){
              this._.editor.insertHtml( '<input class="gendoc-campo-normal" data-identifier="'+ identifier +'" data-nomeinterno="'+ nomeinterno +'" data-camponome="'+ camponome +'" disabled="disabled" readonly="readonly" type="text" value="'+ value +'" />' );
            }
            else{
              if( identifier == '' ){
                alert('Selecione um campo');
              }
              if( nomeinterno == ''){
                alert('Atribua uma identificação ao campo');
              }
              return false;
            }
          }
        };
      });
      editor.addCommand('normal_dialog', new CKEDITOR.dialogCommand('normal_dialog'));
      editor.ui.addButton('Normal', {
        label : 'Campo',
        command : 'normal_dialog',
        icon : '/scripts/fields/icons/fields.png'
      });

      editor.widgets.add( 'gendoc-normal', {

        // any code that needs to be executed when DOM is available.
        init: function() {
        },

        // will be executed every time the widget data is changed
        data: function() {
        },

        // This will ensure that the gendoc-campo dialog window will be opened when creating a new widget or
        // editing an existing one.
        dialog: 'normal_dialog',

        // Allow all HTML elements and classes that this widget requires.
        // Read more about the Advanced Content Filter here:
        // * http://docs.ckeditor.com/#!/guide/dev_advanced_content_filter
        // * http://docs.ckeditor.com/#!/guide/plugin_sdk_integration_with_acf
        allowedContent:
          'input(!gendoc-campo-normal)[!readonly,!disabled,!value,!type,!data-*];'+
            'input(!gendoc-campo-content); h2(!gendoc-campo-title)',

        // Minimum HTML which is required by this widget to work.
        requiredContent: 'input(gendoc-campo-normal)',

        // Check the elements that need to be converted to widgets.
        upcast: function( element ) {
          return element.name == 'input' && element.hasClass( 'gendoc-campo-normal' );
        }
      });

    }
  });

  CKEDITOR.plugins.add('gendoc-espelhado', {
    requires : ['iframedialog', 'widget'],

    init : function(editor) {
      // custom css
      CKEDITOR.config.contentsCss =  'scripts/fields/styles/contents.css';
      // permitir os campos no editor
      editor.filter.allow( 'input(!gendoc-campo-espelhado)[!readonly,!disabled,!value,!type,!data-*];'+
        'input(!gendoc-campo-content); p(*)[*]{*};', 'gendoc-espelhado' );
      // We will start from registering the widget dialog window by calling the standard CKEDITOR.dialog.add method inside the init method of the widget plugin definition.
      var iframeWindow = null;
      var me = this;
      CKEDITOR.dialog.add('espelhado_dialog', function() {
        return {
          title : 'Campo espelhado',
          resizable: CKEDITOR.DIALOG_RESIZE_NONE,
          minWidth: 200,
          minHeight: 230,
          contents : [{
            id : 'iframe',
            label : 'Campo espelhado',
            expand : true,
            elements : [{
              id : 'myIframe',
              type : 'iframe',
              src : 'scripts/fields/dialogs/espelhado.html',
              width : '100%',
              height : '200px',
              onContentLoad : function() {
                var iframe = document.getElementById(this._.frameId);
                iframeWindow = iframe.contentWindow;

                var $iframe = $('#'+this._.frameId).contents();

                // campos disponiveis
                var $relativos = $iframe.find("#camposRelacionados");
                var $sboxes = $( CKEDITOR.instances.editor1.window.getFrame().$ ).contents().find( '.gendoc-campo-normal, .gendoc-campo-referenciado' );

                var result = [];
                $.each($sboxes, function(i, v) {
                  var $this = $(this);
                  if ($.inArray($this.data('nomeinterno'), result) == -1){
                    $relativos.append($("<option />").val($this.data('nomeinterno') +'||'+ $this.data('camponome')).text( $this.data('nomeinterno') +' ('+ $this.data('camponome') +')' ) );
                    result.push($this.data('nomeinterno'));
                  }
                });

                // Try and get the current selected element
                var selection = editor.getSelection();
                var element = ((selection.getSelectedElement() != null) ? $(selection.getSelectedElement().getHtml()) : null);

                // If an image is selected then set the appropriate values
                if (element != null && element[0].tagName == 'INPUT' && element.hasClass('gendoc-campo-espelhado')) {
                  $relativos.val(element.data('nomeinterno') +'||'+ element.data('camponome'));
                }
              }
            }]
          }],
          onOk : function() {

            var myIframe = this.getContentElement('iframe', 'myIframe');
            // can now interrogate values in the iframe, call javascript methods
            // can also call editor methods, e.g. editor.focus(), editor.getSelection()
            var $iframe = $('#'+myIframe.domId).contents();


            // campo espelhado
            var nomeinterno = '';
            var $relativos = $iframe.find("#camposRelacionados");
            var string = $relativos.val();
            if(string != '') {
              string = string.split('||');
              nomeinterno = string[0];
              var camponome = string[1];
              var value = camponome.toUpperCase();
            }

            if( nomeinterno != ''){
              this._.editor.insertHtml( '<input class="gendoc-campo-espelhado" data-camponome="'+ camponome +'" data-nomeinterno="'+ nomeinterno +'" disabled="disabled" readonly="readonly" type="text" value="'+ value +'" />' );
            }
            else{
              alert('Selecione um campo');
              return false;
            }
          }
        };
      });
      editor.addCommand('espelhado_dialog', new CKEDITOR.dialogCommand('espelhado_dialog'));
      editor.ui.addButton('Espelhado', {
        label : 'Campo espelhado',
        command : 'espelhado_dialog',
        icon : '/scripts/fields/icons/espelhado.png'
      });

      editor.widgets.add( 'gendoc-espelhado', {
        // any code that needs to be executed when DOM is available.
        init: function() {
        },

        // will be executed every time the widget data is changed
        data: function() {
        },

        // This will ensure that the gendoc-campo dialog window will be opened when creating a new widget or
        // editing an existing one.
        dialog: 'espelhado_dialog',

        // Allow all HTML elements and classes that this widget requires.
        // Read more about the Advanced Content Filter here:
        // * http://docs.ckeditor.com/#!/guide/dev_advanced_content_filter
        // * http://docs.ckeditor.com/#!/guide/plugin_sdk_integration_with_acf
        allowedContent:
          'input(!gendoc-campo-espelhado)[!readonly,!disabled,!value,!type,!data-*];',

        // Minimum HTML which is required by this widget to work.
        requiredContent: 'input(gendoc-campo-espelhado)',

        // Check the elements that need to be converted to widgets.
        upcast: function( element ) {
          return element.name == 'input' && element.hasClass( 'gendoc-campo-espelhado' );
        }
      });

    }
  });

  CKEDITOR.plugins.add('gendoc-referenciado', {
    requires : ['iframedialog', 'widget'],

    init : function(editor) {
      // custom css
      CKEDITOR.config.contentsCss =  'scripts/fields/styles/contents.css';
      // permitir os campos no editor
      editor.filter.allow( 'input(!gendoc-campo-referenciado)[!readonly,!disabled,!value,!type,!data-*];'+
        'input(!gendoc-campo-content); p(*)[*]{*};', 'gendoc-referenciado' );
      // We will start from registering the widget dialog window by calling the standard CKEDITOR.dialog.add method inside the init method of the widget plugin definition.
      var iframeWindow = null;
      var me = this;
      CKEDITOR.dialog.add('referenciado_dialog', function() {
        return {
          title : 'Campo referenciado',
          resizable: CKEDITOR.DIALOG_RESIZE_NONE,
          minWidth: 200,
          minHeight: 230,
          contents : [{
            id : 'iframe',
            label : 'Campo referenciado',
            expand : true,
            elements : [{
              id : 'myIframe',
              type : 'iframe',
              src : 'scripts/fields/dialogs/referenciado.html',
              width : '100%',
              height : '200px',
              onContentLoad : function() {
                var iframe = document.getElementById(this._.frameId);
                iframeWindow = iframe.contentWindow;

                var $iframe = $('#'+this._.frameId).contents();

                // can now call methods in the iframe window

                var $relativos = $iframe.find("#camposRelacionados");
                var $parametros = $iframe.find('#parametros');
                var $identificacao = $iframe.find('#identificacao');

                // campos disponiveis
//                $.each(GENDOC_EVENT_FIELDS, function(i, v) {
//                  $campos.append($("<option />").val(v.field_form_id).text(v.field_name));
//                });

                var $sboxes = $( CKEDITOR.instances.editor1.window.getFrame().$ ).contents().find( '.gendoc-campo-normal' );
                var result = [];
                $.each($sboxes, function(i, v) {
                  var $this = $(this);
                  if ($.inArray($this.data('nomeinterno'), result) == -1){
                    $relativos.append($("<option />").val($this.data('identifier')).text( $this.data('nomeinterno') +' ('+ $this.data('camponome') +')' ) );
                    result.push($this.data('nomeinterno'));
                  }
                });

                $relativos.change(function(){
                  $iframe.find('#noRef').hide();
                  $iframe.find('#dvParametro').show();
                  var campoObj = getObjects(GENDOC_EVENT_FIELDS, 'field_form_id', $relativos.val());
                  if(campoObj){
                    $.each(campoObj.field_ref, function(i, v) {
                      $parametros.append($("<option />").val(v).text(v));
                    });
                  }
                  else {
                    $iframe.find('#noRef').show();
                    $iframe.find('#dvParametro').hide();
                  }
                });


                // Try and get the current selected element
                var selection = editor.getSelection();
                var element = ((selection.getSelectedElement() != null) ? $(selection.getSelectedElement().getHtml()) : null);

                // If an image is selected then set the appropriate values
                if (element != null && element[0].tagName == 'INPUT' && element.hasClass('gendoc-campo-referenciado')) {
                  $relativos.val(element.data('identifier'));
                  var campoObj = getObjects(GENDOC_EVENT_FIELDS, 'field_form_id', element.data('identifier'));
                  $.each(campoObj.field_ref, function(i, v) {
                    $parametros.append($("<option />").val(v).text(v));
                  });
                  $parametros.val(element.data('rel'));
                  $identificacao.val(element.data('nomeinterno'));

                }
              }
            }]
          }],
          onOk : function() {

            var myIframe = this.getContentElement('iframe', 'myIframe');
            // can now interrogate values in the iframe, call javascript methods
            // can also call editor methods, e.g. editor.focus(), editor.getSelection()
            var $iframe = $('#'+myIframe.domId).contents();


            var $relativos = $iframe.find("#camposRelacionados");

            var $parametros = $iframe.find('#parametros');
            var $identificacao = $iframe.find('#identificacao');

            var campoObj = getObjects(GENDOC_EVENT_FIELDS, 'field_form_id', $relativos.val());
            var identifier = campoObj.field_form_id;
            var camponome = campoObj.field_name;
            var value = camponome.toUpperCase() +' ['+ $parametros.val() +']';

            var nomeinterno = $identificacao.val();

            if( identifier != '' && nomeinterno != ''){
              this._.editor.insertHtml( '<input class="gendoc-campo-referenciado" data-identifier="'+ identifier +'" data-rel="'+ $parametros.val() +'" data-camponome="'+ camponome +'" data-nomeinterno="'+ nomeinterno +'" disabled="disabled" readonly="readonly" type="text" value="'+ value +'" />' );
            }
            else{
              if( identifier == '' ){
                alert('Selecione um campo');
              }
              if( nomeinterno == ''){
                alert('Atribua uma identificação ao campo');
              }
              return false;
            }


          }
        };
      });
      editor.addCommand('referenciado_dialog', new CKEDITOR.dialogCommand('referenciado_dialog'));
      editor.ui.addButton('referenciado', {
        label : 'Campo referenciado',
        command : 'referenciado_dialog',
        icon : '/scripts/fields/icons/referenciado.png'
      });

      editor.widgets.add( 'gendoc-referenciado', {
        // any code that needs to be executed when DOM is available.
        init: function() {
        },

        // will be executed every time the widget data is changed
        data: function() {
        },

        // This will ensure that the gendoc-campo dialog window will be opened when creating a new widget or
        // editing an existing one.
        dialog: 'referenciado_dialog',

        // Allow all HTML elements and classes that this widget requires.
        // Read more about the Advanced Content Filter here:
        // * http://docs.ckeditor.com/#!/guide/dev_advanced_content_filter
        // * http://docs.ckeditor.com/#!/guide/plugin_sdk_integration_with_acf
        allowedContent:
          'input(!gendoc-campo-referenciado)[!readonly,!disabled,!value,!type,!data-*];',

        // Minimum HTML which is required by this widget to work.
        requiredContent: 'input(gendoc-campo-referenciado)',

        // Check the elements that need to be converted to widgets.
        upcast: function( element ) {
          return element.name == 'input' && element.hasClass( 'gendoc-campo-referenciado' );
        }
      });

    }
  });
})();

